__kernel void rast_spans(__global float* span_x_1, __global float* span_x_2, __global float* span_z_1, __global float* span_z_2, __global float* span_u_x_1, __global float* span_u_y_1, __global float* span_u_x_2, __global float* span_u_y_2, __global int* span_y, uint num_spans, __global float* texture, uint texture_width, uint texture_height, uint render_width, uint render_height, __global float* render_buffer, __global float* z_buffer) {
	int i = get_global_id(0);
	float x_1 = span_x_1[i];
	float x_2 = span_x_2[i];
	float z_1 = span_z_1[i];
	float z_2 = span_z_2[i];
	float u_x_1 = span_u_x_1[i];
	float u_y_1 = span_u_y_1[i];
	float u_x_2 = span_u_x_2[i];
	float u_y_2 = span_u_y_2[i];
	int y = span_y[i];
	int x_diff = (int) x_2 - (int) x_1;
	if (x_diff == 0) {
		return;
	}
	float t_diff_x = u_x_2 - u_x_1;
	float t_diff_y = u_y_2 - u_y_1;
	float factor = (float) 0;
	float factor_step = (float) 1 / (float) x_diff;
	for (int x = x_1; x < x_2; x++) {
		float z_depth = z_1 + ((x - x_1) / (x_2 - x_1)) * (z_2 - z_1);
		int z_index = y * render_width + x;
		if (z_depth < z_buffer[z_index]) {
			float t_x = u_x_1;
			float t_y = u_y_1;
			t_x += t_diff_x * factor;
			t_y += t_diff_y * factor;
			float uv_x = t_x * (float) texture_width;
			float uv_y = ((float) 1 - t_y) * (float) texture_height;
			int t_index = ((int) uv_y * texture_width + (int) uv_x) * 4;
			int o_index = (y * render_width + x) * 3;
			render_buffer[o_index] = texture[t_index];
			render_buffer[o_index + 1] = texture[t_index + 1];
			render_buffer[o_index + 2] = texture[t_index + 2];
			z_buffer[z_index] = z_depth;
			factor += factor_step;
		}
	}
}
