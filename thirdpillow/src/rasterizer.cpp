/*
 * rasterizer.cpp
 *
 *  Created on: Apr 29, 2014
 *      Author: jwpilly
 */

#include "rasterizer.h"

rasterizer::rasterizer(color* c) {
	this->default_color = *c;
	initialize_cl();
}

rasterizer::rasterizer(color* c, bool cache) {
	this->default_color = *c;
	this->cache = cache;
	initialize_cl();
}

void rasterizer::initialize_cl() {
	error = clGetPlatformIDs(0, NULL, &num_platforms);
	if (error != CL_SUCCESS) {
		printf("unable to get platorms\n");
		exit(-1);
	}
	platforms = (cl_platform_id*)malloc(sizeof(cl_platform_id) * num_platforms);
	error = clGetPlatformIDs(num_platforms, platforms, NULL);
	error = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_DEFAULT, 0, NULL, &num_devices);
	if (error != CL_SUCCESS) {
		printf("unable to get devices\n");
		exit(-1);
	}
	devices = (cl_device_id*)malloc(sizeof(cl_device_id) * num_devices);
	error = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_DEFAULT, num_devices, devices, NULL);
	context = clCreateContext(NULL, num_devices, devices, NULL, NULL, &error);
	if (error != CL_SUCCESS) {
		printf("unable to create context\n");
		exit(-1);
	}
	command_queue = clCreateCommandQueue(context, *devices, 0, &error);
	if (error != CL_SUCCESS) {
		printf("unable to create command queue\n");
		exit(-1);
	}
	int k_length = 0;
	const char* kernel_src = putils::read_file("src/drawspan_kernel.cl", &k_length);
	program = clCreateProgramWithSource(context, 1, &kernel_src, NULL, &error);
	if (error != CL_SUCCESS) {
		printf("unable to create program\n");
		exit(-1);
	}
	clBuildProgram(program, num_devices, devices, NULL, NULL, NULL);
	if (error != CL_SUCCESS) {
		printf("unable to build program\n");
		exit(-1);
	}
	char buffer[2048];
	size_t length;
	clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &length);
	printf((const char*)buffer);
	printf("\n");
	kernel = clCreateKernel(program, "rast_spans", &error);
	if (error != CL_SUCCESS) {
		printf("unable to find kernel\n");
		printf("error: %d\n", error);
		exit(-1);
	}
	printf("opencl kernel built and initialized\n");
	span_x_1d = clCreateBuffer(context, CL_MEM_READ_ONLY, (rasterizer::MAX_SPANS) * sizeof(float), NULL, NULL);
	span_x_2d = clCreateBuffer(context, CL_MEM_READ_ONLY, (rasterizer::MAX_SPANS) * sizeof(float), NULL, NULL);
	span_z_1d = clCreateBuffer(context, CL_MEM_READ_ONLY, (rasterizer::MAX_SPANS) * sizeof(float), NULL, NULL);
	span_z_2d = clCreateBuffer(context, CL_MEM_READ_ONLY, (rasterizer::MAX_SPANS) * sizeof(float), NULL, NULL);
	span_u_x_1d = clCreateBuffer(context, CL_MEM_READ_ONLY, (rasterizer::MAX_SPANS) * sizeof(float), NULL, NULL);
	span_u_y_1d = clCreateBuffer(context, CL_MEM_READ_ONLY, (rasterizer::MAX_SPANS) * sizeof(float), NULL, NULL);
	span_u_x_2d = clCreateBuffer(context, CL_MEM_READ_ONLY, (rasterizer::MAX_SPANS) * sizeof(float), NULL, NULL);
	span_u_y_2d = clCreateBuffer(context, CL_MEM_READ_ONLY, (rasterizer::MAX_SPANS) * sizeof(float), NULL, NULL);
	span_yd = clCreateBuffer(context, CL_MEM_READ_ONLY, (rasterizer::MAX_SPANS) * sizeof(int), NULL, NULL);
	textured = clCreateBuffer(context, CL_MEM_READ_ONLY, (rasterizer::MAX_TEXTURE_WIDTH * rasterizer::MAX_TEXTURE_HEIGHT) * 4 * sizeof(float), NULL, NULL);
	render_bufferd = clCreateBuffer(context, CL_MEM_WRITE_ONLY, (1920 * 1080) * 3 * sizeof(float), NULL, NULL);
	z_bufferd = clCreateBuffer(context, CL_MEM_READ_WRITE, (1920 * 1080) * sizeof(float), NULL, NULL);
	cl_uint c_render_width = 1920;
	cl_uint c_render_height = 1080;
	clSetKernelArg(kernel, 13, sizeof(cl_uint), &c_render_width);
	clSetKernelArg(kernel, 14, sizeof(cl_uint), &c_render_height);
	cl_uint c_max_texture_width = 1000;
	cl_uint c_max_texture_height = 1000;
	clSetKernelArg(kernel, 15, sizeof(cl_mem), &render_bufferd);

}

color* rasterizer::get_default_color() {
	return &this->default_color;
}

void rasterizer::set_default_color(color* c) {
	this->default_color = *c;
}

void rasterizer::draw_span(screen* s, span a, image* texture, int y) {
	int x_diff = (int)a.get_x_2() - (int)a.get_x_1();
	if (x_diff == 0) {
		return;
	}
	vector2 t_diff = a.get_uv_b();
	t_diff.subtract(a.get_uv_a());
	float factor = (float)0;
	float factor_step = (float)1 / (float)x_diff;
	for (int x = a.get_x_1(); x < a.get_x_2(); x++) {
		float z_depth = putils::linear_interpolate(a.get_z_1(), a.get_z_2(), (x - a.get_x_1()) / (a.get_x_2() - a.get_x_1()));
		if (z_depth < s->get_z(x, y)) {
			vector2 t = a.get_uv_a();
			vector2 t_1 = t_diff;
			t_1.multiply(factor);
			t.add(t_1);
			float uv_x = t.get_x() * (float)texture->get_width();
			float uv_y = ((float)1 - t.get_y()) * (float)texture->get_height();
			color i = texture->get_color((int)uv_x, (int)uv_y);
			s->set_pixel(x, y, i);
			s->set_z(x, y, z_depth);
			factor += factor_step;
		}
	}
}

void rasterizer::draw_edge_span(screen* s, edge a, edge b, image* texture) {
	float y_diff_1 = (float)(a.get_b().get_y() - a.get_a().get_y());
	if (y_diff_1 == (float)0) {
		return;
	}
	float y_diff_2 = (float)(b.get_b().get_y() - b.get_a().get_y());
	if (y_diff_2 == (float)0) {
		return;
	}
	float x_diff_1 = (float)(a.get_b().get_x() - a.get_a().get_x());
	float x_diff_2 = (float)(b.get_b().get_x() - b.get_a().get_x());
	vector2 e_1 = a.get_uv_b();
	e_1.subtract(a.get_uv_a());
	vector2 e_2 = b.get_uv_b();
	e_2.subtract(b.get_uv_a());
	float factor_1 = (float)(b.get_a().get_y() - a.get_a().get_y()) / y_diff_1;
	float factor_step_1 = (float)1 / y_diff_1;
	float factor_2 = (float)0;
	float factor_step_2 = (float)1 / y_diff_2;
	for (int y = (int)b.get_a().get_y(); y < (int)b.get_b().get_y(); y++) {
		vector2 c = a.get_uv_a();
		vector2 c_1 = e_1;
		c_1.multiply(factor_1);
		c.add(c_1);
		int c_x_1 = (int)a.get_a().get_x() + (int)(x_diff_1 * factor_1);
		vector2 d = b.get_uv_a();
		vector2 d_1 = e_2;
		d_1.multiply(factor_2);
		d.add(d_1);
		int c_x_2 = (int)b.get_a().get_x() + (int)(x_diff_2 * factor_2);
		float z_1 = putils::linear_interpolate(a.get_z_a(), b.get_z_a(), ((float)y - b.get_a().get_y()) / (b.get_b().get_y() - b.get_a().get_y()));
		float z_2 = putils::linear_interpolate(a.get_z_b(), b.get_z_b(), ((float)y - b.get_a().get_y()) / (b.get_b().get_y() - b.get_a().get_y()));
		span sp(c, z_1, c_x_1, d, z_2, c_x_2);
		//draw_span(s, sp, texture, y);
		p_span queued_span;
		queued_span.sp = sp;
		queued_span.texture = texture;
		queued_span.y = y;
		screen_spans.push_back(queued_span);
		factor_1 += factor_step_1;
		factor_2 += factor_step_2;
	}
}

void rasterizer::draw_triangle_wire_color(screen* s, vector2 a, color a_color, vector2 b, color b_color, vector2 c, color c_color) {
	draw_line_color(s, a, a_color, b, b_color);
	draw_line_color(s, b, b_color, c, c_color);
	draw_line_color(s, c, c_color, a, a_color);
}

void rasterizer::draw_triangle_wire(screen* s, vector2 a, vector2 b, vector2 c) {
	draw_line_color(s, a, b, this->default_color);
	draw_line_color(s, b, c, this->default_color);
	draw_line_color(s, c, a, this->default_color);
}

void rasterizer::draw_triangle_wire(screen* s, triangle2 t) {
	vector2* v_t = t.get_vertices();
	draw_triangle_wire(s, v_t[0], v_t[1], v_t[2]);
	delete[] v_t;
}

void rasterizer::draw_line_color(screen* s, vector2 a, vector2 b, color c) {
	float x_diff = b.get_x() - a.get_x();
	float y_diff = b.get_y() - a.get_y();
	if (x_diff == 0 && y_diff == 0) {
		s->set_pixel(a.get_x(), a.get_y(), c);
		return;
	}
	if (fabs(x_diff) > fabs(y_diff)) {
		float x_min, x_max;
		if (a.get_x() < b.get_x()) {
			x_min = a.get_x();
			x_max = b.get_x();
		}
		else {
			x_min = b.get_x();
			x_max = a.get_x();
		}
		float slope = y_diff / x_diff;
		for (float x = x_min; x <= x_max; x += (float)1) {
			float y = a.get_y() + ((x - a.get_x()) * slope);
			s->set_pixel((int)x, (int)y, c);
		}
	}
	else {
		float y_min, y_max;
		if (a.get_y() < b.get_y()) {
			y_min = a.get_y();
			y_max = b.get_y();
		}
		else {
			y_min = b.get_y();
			y_max = a.get_y();
		}
		float slope = x_diff / y_diff;
		for (float y = y_min; y <= y_max; y += (float)1) {
			float x = a.get_x() + ((y - a.get_y()) * slope);
			s->set_pixel((int)x, (int)y, c);
		}
	}
}

void rasterizer::draw_line_color(screen* s, vector2 a, color a_color, vector2 b, color b_color) {
	float x_diff = b.get_x() - a.get_x();
	float y_diff = b.get_y() - a.get_y();
	if (x_diff == 0 && y_diff == 0) {
		s->set_pixel(a.get_x(), a.get_y(), a_color);
		return;
	}
	if (fabs(x_diff) > fabs(y_diff)) {
		float x_min, x_max;
		if (a.get_x() < b.get_x()) {
			x_min = a.get_x();
			x_max = b.get_x();
		}
		else {
			x_min = b.get_x();
			x_max = a.get_x();
		}
		float slope = y_diff / x_diff;
		for (float x = x_min; x <= x_max; x += (float)1) {
			float y = a.get_y() + ((x - a.get_x()) * slope);
			color c = a_color;
			float c_s = (x - a.get_x()) / x_diff;
			color d = b_color;
			d.subtract(a_color);
			d.multiply(c_s);
			c.add(d);
			s->set_pixel((int)x, (int)y, c);
		}
	}
	else {
		float y_min, y_max;
		if (a.get_y() < b.get_y()) {
			y_min = a.get_y();
			y_max = b.get_y();
		}
		else {
			y_min = b.get_y();
			y_max = a.get_y();
		}
		float slope = x_diff / y_diff;
		for (float y = y_min; y <= y_max; y += (float)1) {
			float x = a.get_x() + ((y - a.get_y()) * slope);
			color c = a_color;
			float c_s = ((y - a.get_y()) / y_diff);
			color d = b_color;
			d.subtract(a_color);
			d.multiply(c_s);
			c.add(d);
			s->set_pixel((int)x, (int)y, c);
		}
	}
}

void rasterizer::draw_face_wire(screen* s, face* f, matrix4 mt) {
	triangle2 flat = f->get_triangle().flatten(mt);
	this->draw_triangle_wire(s, flat);
}

void rasterizer::draw_face_textured(screen* s, face* f, image* texture, matrix4 mt) {
	float z_depth[3];
	triangle2 flat = f->get_triangle().flatten_z(mt, z_depth); //get local z value
	vector2* vertices = flat.get_vertices();
	vector2* uvs = f->get_uvs();
	p_face_texture p_face;
	if (cache) {
		p_face_2[f] = p_face_1[f];
		p_face.z_a = z_depth[0];
		p_face.z_b = z_depth[1];
		p_face.z_c = z_depth[2];
		p_face.v_a = vertices[0];
		p_face.v_b = vertices[1];
		p_face.v_c = vertices[2];
		p_face.u_a = uvs[0];
		p_face.u_b = uvs[1];
		p_face.u_c = uvs[2];
		p_face.texture = texture;
		p_face_1[f] = p_face;
	}
	edge edges[3];
	edge a(vertices[0], uvs[0], z_depth[0], vertices[1], uvs[1], z_depth[1]);
	edge b(vertices[1], uvs[1], z_depth[1], vertices[2], uvs[2], z_depth[2]);
	edge c(vertices[2], uvs[2], z_depth[2], vertices[0], uvs[0], z_depth[0]);
	edges[0] = a;
	edges[1] = b;
	edges[2] = c;
	float max_length = (float)0;
	int long_edge = 0;
	for (int i = 0; i < 3; i++) {
		float length = edges[i].get_b().get_y() - edges[i].get_a().get_y();
		if (length > max_length) {
			max_length = length;
			long_edge = i;
		}
	}
	int short_1 = (long_edge + 1) % 3;
	int short_2 = (long_edge + 2) % 3;
	draw_edge_span(s, edges[long_edge], edges[short_1], texture);
	draw_edge_span(s, edges[long_edge], edges[short_2], texture);
	delete[] vertices;
}

void rasterizer::draw_mesh_wire(screen* s, mesh m, matrix4 mt) {
	for (int i = 0; i < m.faces.size(); i++) {
		draw_face_wire(s, &m.faces[i], mt);
	}
}

void rasterizer::draw_mesh_normals(screen* s, mesh m, matrix4 mt) {
	for (int i = 0; i < m.faces.size(); i++) {
		vector3 local_normal = m.faces[i].get_triangle().get_normal();
		vector3 barycenter = m.faces[i].get_triangle().get_center();
		vector3 normal = barycenter;
		normal.add(local_normal);
		vector4 a4(barycenter);
		vector4 b4(normal);
		a4.multiply_first(mt);
		b4.multiply_first(mt);
		a4.set_x(a4.get_x() / a4.get_w());
		a4.set_y(a4.get_y() / a4.get_w());
		a4.set_z(a4.get_z() / a4.get_w());
		a4.set_x(a4.get_x() / a4.get_z());
		a4.set_y(a4.get_y() / a4.get_z());
		b4.set_x(b4.get_x() / b4.get_w());
		b4.set_y(b4.get_y() / b4.get_w());
		b4.set_z(b4.get_z() / b4.get_w());
		b4.set_x(b4.get_x() / b4.get_z());
		b4.set_y(b4.get_y() / b4.get_z());
		float p_x_a = ((a4.get_x() / (float)2) + (float)0.5) * (float)transform::get_camera()->get_render_width();
		float p_y_a = ((a4.get_y() / (float)2) + (float)0.5) * (float)transform::get_camera()->get_render_height();
		float p_x_b = ((b4.get_x() / (float)2) + (float)0.5) * (float)transform::get_camera()->get_render_width();
		float p_y_b = ((b4.get_y() / (float)2) + (float)0.5) * (float)transform::get_camera()->get_render_height();
		vector2 a(p_x_a, p_y_a);
		vector2 b(p_x_b, p_y_b);
		color white(1, 1, 1, 1);
		draw_line_color(s, a, b, white);
	}
}

void rasterizer::draw_mesh_textured(screen *s, mesh m, image* texture, matrix4 mt) {
	for (int i = 0; i < m.faces.size(); i++) {
		draw_face_textured(s, &m.faces[i], texture, mt);
	}
}

void rasterizer::draw_mesh_textured_cull(screen *s, mesh m, image* texture, matrix4 mt) {
#pragma omp parallel for
	for (int i = 0; i < m.faces.size(); i++) {
		float dot = m.faces[i].get_triangle().get_normal().dot_product(*transform::get_camera()->get_forward());
		if (dot <= 0) {
			draw_face_textured(s, &m.faces[i], texture, mt);
		}
	}
}

void rasterizer::draw_mesh_wire_cull(screen* s, mesh m, matrix4 mt) {
	for (int i = 0; i < m.faces.size(); i++) {
		float dot = m.faces[i].get_triangle().get_normal().dot_product(*transform::get_camera()->get_forward());
		if (dot <= 0.5) {
			draw_face_wire(s, &m.faces[i], mt);
		}
	}
}

void rasterizer::draw_image(screen* s, image texture) {
	for (int x = 0; x < texture.get_width(); x++) {
		for (int y = 0; y < texture.get_height(); y++) {
			s->set_pixel(x, s->get_height() - y, texture.get_color(x, y));
		}
	}
}

void rasterizer::set_pixel(screen* s, vector2 p, color c) {
	s->set_pixel((int)p.get_x(), (int)p.get_y(), c);
}

void rasterizer::extrapolate_face_it(screen* s, std::map<face*, p_face_texture>::iterator it) {
	// yes i know this is doing it again
	p_face_texture f_1 = it->second;
	p_face_texture f_2 = p_face_2[it->first];
	// use a loop to do this stuff later
	//float elapsed_t = float(f_1.f_time - f_2.f_time);
	//float elapsed_t_2 = float(clock() - f_1.f_time);
	//float time_ratio = elapsed_t_2 / elapsed_t;
	float delta_z[3];
	delta_z[0] = f_1.z_a - f_2.z_a;
	delta_z[1] = f_1.z_b - f_2.z_b;
	delta_z[2] = f_1.z_c - f_2.z_c;
	vector2 delta_v[3];
	delta_v[0] = f_1.v_a;
	delta_v[0].subtract(f_2.v_a);
	delta_v[1] = f_1.v_b;
	delta_v[1].subtract(f_2.v_b);
	delta_v[2] = f_1.v_c;
	delta_v[2].subtract(f_2.v_c);
	//delta_v[0].multiply(time_ratio);
	//delta_v[1].multiply(time_ratio);
	//delta_v[2].multiply(time_ratio);
	float e_z[3];
	e_z[0] = f_1.z_a + delta_z[0];
	e_z[1] = f_1.z_b + delta_z[1];
	e_z[2] = f_1.z_c + delta_z[2];
	vector2 e_v[3];
	e_v[0] = f_1.v_a;
	e_v[0].add(delta_v[0]);
	e_v[1] = f_1.v_b;
	e_v[1].add(delta_v[1]);
	e_v[2] = f_1.v_c;
	e_v[2].add(delta_v[2]);
	//delta_z[0] *= time_ratio;
	//delta_z[1] *= time_ratio;
	//delta_z[2] *= time_ratio;
	edge edges[3];
	edge a(e_v[0], f_1.u_a, e_z[0], e_v[1], f_1.u_b, e_z[1]);
	edge b(e_v[1], f_1.u_b, e_z[1], e_v[2], f_1.u_c, e_z[2]);
	edge c(e_v[2], f_1.u_c, e_z[2], e_v[0], f_1.u_a, e_z[0]);
	edges[0] = a;
	edges[1] = b;
	edges[2] = c;
	float max_length = (float)0;
	int long_edge = 0;
	for (int i = 0; i < 3; i++) {
		float length = edges[i].get_b().get_y() - edges[i].get_a().get_y();
		if (length > max_length) {
			max_length = length;
			long_edge = i;
		}
	}
	int short_1 = (long_edge + 1) % 3;
	int short_2 = (long_edge + 2) % 3;
	draw_edge_span(s, edges[long_edge], edges[short_1], f_1.texture);
	draw_edge_span(s, edges[long_edge], edges[short_2], f_1.texture);
}

void rasterizer::draw_all_spans(screen* s) {
#pragma omp parallel
	for (int i = 0; i < screen_spans.size(); i++) {
		p_span d_span = screen_spans[i];
		draw_span(s, d_span.sp, d_span.texture, d_span.y);
	}
	screen_spans.clear();
}

void rasterizer::cl_draw_all_spans(screen* s) {
	float* span_x_1l = new float[screen_spans.size()];
	float* span_x_2l = new float[screen_spans.size()];
	float* span_z_1l = new float[screen_spans.size()];
	float* span_z_2l = new float[screen_spans.size()];
	float* span_u_x_1l = new float[screen_spans.size()];
	float* span_u_y_1l = new float[screen_spans.size()];
	float* span_u_x_2l = new float[screen_spans.size()];
	float* span_u_y_2l = new float[screen_spans.size()];
	int* span_yl = new int[screen_spans.size()];
	for (int i = 0; i < screen_spans.size(); i++) {
		span_x_1l[i] = screen_spans[i].sp.get_x_1();
		span_x_2l[i] = screen_spans[i].sp.get_x_2();
		span_z_1l[i] = screen_spans[i].sp.get_z_1();
		span_z_2l[i] = screen_spans[i].sp.get_z_2();
		span_u_x_1l[i] = screen_spans[i].sp.get_uv_a().get_x();
		span_u_y_1l[i] = screen_spans[i].sp.get_uv_a().get_y();
		span_u_x_2l[i] = screen_spans[i].sp.get_uv_b().get_x();
		span_u_y_2l[i] = screen_spans[i].sp.get_uv_b().get_y();
		span_yl[i] = screen_spans[i].y;
	}
	cl_uint num_spans = screen_spans.size();
	image* texturel = screen_spans[0].texture; // all one texture for now
	cl_uint i_width = texturel->get_width();
	cl_uint i_height = texturel->get_height();
	float* texture_data = texturel->get_pixel_data();
	clEnqueueWriteBuffer(command_queue, span_x_1d, CL_FALSE, 0, screen_spans.size() * sizeof(float), span_x_1l, 0, NULL, NULL);
	clEnqueueWriteBuffer(command_queue, span_x_2d, CL_FALSE, 0, screen_spans.size() * sizeof(float), span_x_2l, 0, NULL, NULL);
	clEnqueueWriteBuffer(command_queue, span_z_1d, CL_FALSE, 0, screen_spans.size() * sizeof(float), span_z_1l, 0, NULL, NULL);
	clEnqueueWriteBuffer(command_queue, span_z_2d, CL_FALSE, 0, screen_spans.size() * sizeof(float), span_z_2l, 0, NULL, NULL);
	clEnqueueWriteBuffer(command_queue, span_u_x_1d, CL_FALSE, 0, screen_spans.size() * sizeof(float), span_u_x_1l, 0, NULL, NULL);
	clEnqueueWriteBuffer(command_queue, span_u_y_1d, CL_FALSE, 0, screen_spans.size() * sizeof(float), span_u_y_1l, 0, NULL, NULL);
	clEnqueueWriteBuffer(command_queue, span_u_x_2d, CL_FALSE, 0, screen_spans.size() * sizeof(float), span_u_x_2l, 0, NULL, NULL);
	clEnqueueWriteBuffer(command_queue, span_u_y_2d, CL_FALSE, 0, screen_spans.size() * sizeof(float), span_u_y_2l, 0, NULL, NULL);
	clEnqueueWriteBuffer(command_queue, span_yd, CL_FALSE, 0, screen_spans.size() * sizeof(int), span_yl, 0, NULL, NULL);
	clEnqueueWriteBuffer(command_queue, textured, CL_FALSE, 0, texturel->get_width() * texturel->get_height() * 4 * sizeof(float), texture_data, 0, NULL, NULL);
	clEnqueueWriteBuffer(command_queue, z_bufferd, CL_FALSE, 0, s->get_width() * s->get_height() * sizeof(float), s->get_z_buffer(), 0, NULL, NULL);
	clEnqueueWriteBuffer(command_queue, render_bufferd, CL_FALSE, 0, s->get_width() * s->get_height() * 3 * sizeof(float), empty_screen, 0, NULL, NULL);
	clFinish(command_queue);
	clSetKernelArg(kernel, 0, sizeof(cl_mem), &span_x_1d);
	clSetKernelArg(kernel, 1, sizeof(cl_mem), &span_x_2d);
	clSetKernelArg(kernel, 2, sizeof(cl_mem), &span_z_1d);
	clSetKernelArg(kernel, 3, sizeof(cl_mem), &span_z_2d);
	clSetKernelArg(kernel, 4, sizeof(cl_mem), &span_u_x_1d);
	clSetKernelArg(kernel, 5, sizeof(cl_mem), &span_u_y_1d);
	clSetKernelArg(kernel, 6, sizeof(cl_mem), &span_u_x_2d);
	clSetKernelArg(kernel, 7, sizeof(cl_mem), &span_u_y_2d);
	clSetKernelArg(kernel, 8, sizeof(cl_mem), &span_yd);
	clSetKernelArg(kernel, 9, sizeof(cl_uint), &num_spansd);
	clSetKernelArg(kernel, 10, sizeof(cl_mem), &textured);
	clSetKernelArg(kernel, 11, sizeof(cl_uint), &i_width);
	clSetKernelArg(kernel, 12, sizeof(cl_uint), &i_height);
	clSetKernelArg(kernel, 15, sizeof(cl_mem), &render_bufferd);
	clSetKernelArg(kernel, 16, sizeof(cl_mem), &z_bufferd);
	size_t range_size = (size_t)screen_spans.size();
	clEnqueueNDRangeKernel(command_queue, kernel, 1, 0, &range_size, 0, 0, NULL, NULL);
	clFinish(command_queue);
	clEnqueueReadBuffer(command_queue, render_bufferd, CL_TRUE, 0, s->get_width() * s->get_height() * 3 * sizeof(float), s->get_buffer(), 0, NULL, NULL);
	clFinish(command_queue);
	delete[] span_x_1l;
	delete[] span_x_2l;
	delete[] span_z_1l;
	delete[] span_z_2l;
	delete[] span_u_x_1l;
	delete[] span_u_y_1l;
	delete[] span_u_x_2l;
	delete[] span_u_y_2l;
	delete[] span_yl;
	screen_spans.clear();
}

void rasterizer::cpu_draw_all_spans(screen * s)
{
	float* span_x_1l = new float[screen_spans.size()];
	float* span_x_2l = new float[screen_spans.size()];
	float* span_z_1l = new float[screen_spans.size()];
	float* span_z_2l = new float[screen_spans.size()];
	float* span_u_x_1l = new float[screen_spans.size()];
	float* span_u_y_1l = new float[screen_spans.size()];
	float* span_u_x_2l = new float[screen_spans.size()];
	float* span_u_y_2l = new float[screen_spans.size()];
	int* span_yl = new int[screen_spans.size()];
	for (int i = 0; i < screen_spans.size(); i++) {
		span_x_1l[i] = screen_spans[i].sp.get_x_1();
		span_x_2l[i] = screen_spans[i].sp.get_x_2();
		span_z_1l[i] = screen_spans[i].sp.get_z_1();
		span_z_2l[i] = screen_spans[i].sp.get_z_2();
		span_u_x_1l[i] = screen_spans[i].sp.get_uv_a().get_x();
		span_u_y_1l[i] = screen_spans[i].sp.get_uv_a().get_y();
		span_u_x_2l[i] = screen_spans[i].sp.get_uv_b().get_x();
		span_u_y_2l[i] = screen_spans[i].sp.get_uv_b().get_y();
		span_yl[i] = screen_spans[i].y;
	}
	cl_uint num_spans = screen_spans.size();
	image* texturel = screen_spans[0].texture; // all one texture for now
	cl_uint i_width = texturel->get_width();
	cl_uint i_height = texturel->get_height();
	float* texture_data = texturel->get_pixel_data();
	for (int i = 0; i < screen_spans.size(); i++) {
		cpu_rast_spans(i, span_x_1l, span_x_2l, span_z_1l, span_z_2l, span_u_x_1l, span_u_y_1l, span_u_x_2l, span_u_y_2l, span_yl, num_spans, texture_data, i_width, i_height, s->get_width(), s->get_height(), s->get_buffer(), s->get_z_buffer());
	}
	delete[] span_x_1l;
	delete[] span_x_2l;
	delete[] span_z_1l;
	delete[] span_z_2l;
	delete[] span_u_x_1l;
	delete[] span_u_y_1l;
	delete[] span_u_x_2l;
	delete[] span_u_y_2l;
	delete[] span_yl;
	screen_spans.clear();
}

void rasterizer::mp_draw_all_spans(screen * s)
{
	float* span_x_1l = new float[screen_spans.size()];
	float* span_x_2l = new float[screen_spans.size()];
	float* span_z_1l = new float[screen_spans.size()];
	float* span_z_2l = new float[screen_spans.size()];
	float* span_u_x_1l = new float[screen_spans.size()];
	float* span_u_y_1l = new float[screen_spans.size()];
	float* span_u_x_2l = new float[screen_spans.size()];
	float* span_u_y_2l = new float[screen_spans.size()];
	int* span_yl = new int[screen_spans.size()];
	for (int i = 0; i < screen_spans.size(); i++) {
		span_x_1l[i] = screen_spans[i].sp.get_x_1();
		span_x_2l[i] = screen_spans[i].sp.get_x_2();
		span_z_1l[i] = screen_spans[i].sp.get_z_1();
		span_z_2l[i] = screen_spans[i].sp.get_z_2();
		span_u_x_1l[i] = screen_spans[i].sp.get_uv_a().get_x();
		span_u_y_1l[i] = screen_spans[i].sp.get_uv_a().get_y();
		span_u_x_2l[i] = screen_spans[i].sp.get_uv_b().get_x();
		span_u_y_2l[i] = screen_spans[i].sp.get_uv_b().get_y();
		span_yl[i] = screen_spans[i].y;
	}
	cl_uint num_spans = screen_spans.size();
	image* texturel = screen_spans[0].texture; // all one texture for now
	cl_uint i_width = texturel->get_width();
	cl_uint i_height = texturel->get_height();
	float* texture_data = texturel->get_pixel_data();
#pragma omp parallel for
	for (int i = 0; i < screen_spans.size(); i++) {
		cpu_rast_spans(i, span_x_1l, span_x_2l, span_z_1l, span_z_2l, span_u_x_1l, span_u_y_1l, span_u_x_2l, span_u_y_2l, span_yl, num_spans, texture_data, i_width, i_height, s->get_width(), s->get_height(), s->get_buffer(), s->get_z_buffer());
	}
	delete[] span_x_1l;
	delete[] span_x_2l;
	delete[] span_z_1l;
	delete[] span_z_2l;
	delete[] span_u_x_1l;
	delete[] span_u_y_1l;
	delete[] span_u_x_2l;
	delete[] span_u_y_2l;
	delete[] span_yl;
	screen_spans.clear();
}

// TODO try this with just jagged arrays
void rasterizer::extrapolate_frame(screen* s) {
#pragma omp parallel
	for (auto it = p_face_1.begin(); it != p_face_1.end(); it++) {
#ifdef __APPLE__
#pragma omp task firstprivate(it)
		extrapolate_face_it(s, it);
#pragma omp taskwait
#else
		extrapolate_face_it(s, it);
#endif
	}
}
void rasterizer::cpu_rast_spans(int i, float* span_x_1, float* span_x_2, float* span_z_1, float* span_z_2, float* span_u_x_1, float* span_u_y_1, float* span_u_x_2, float* span_u_y_2, int* span_y, int num_spans, float* texture, int texture_width, int texture_height, int render_width, int render_height, float* render_buffer, float* z_buffer) {
	float x_1 = span_x_1[i];
	float x_2 = span_x_2[i];
	float z_1 = span_z_1[i];
	float z_2 = span_z_2[i];
	float u_x_1 = span_u_x_1[i];
	float u_y_1 = span_u_y_1[i];
	float u_x_2 = span_u_x_2[i];
	float u_y_2 = span_u_y_2[i];
	int y = span_y[i];
	int x_diff = (int)x_2 - (int)x_1;
	if (x_diff == 0) {
		return;
	}
	float t_diff_x = u_x_2 - u_x_1;
	float t_diff_y = u_y_2 - u_y_1;
	float factor = (float)0;
	float factor_step = (float)1 / (float)x_diff;
	for (int x = x_1; x < x_2; x++) {
		float z_depth = z_1 + ((x - x_1) / (x_2 - x_1)) * (z_2 - z_1);
		int z_index = y * render_width + x;
		if (z_index < render_width * render_height && z_index < 0 && z_depth < z_buffer[z_index]) {
			float t_x = u_x_1;
			float t_y = u_y_1;
			t_x += t_diff_x * factor;
			t_y += t_diff_y * factor;
			float uv_x = t_x * (float)texture_width;
			float uv_y = ((float)1 - t_y) * (float)texture_height;
			int t_index = (((int)uv_y * texture_width + (int)uv_x) * 4);
			int o_index = (y * render_width + x) * 3;
			render_buffer[o_index] = texture[t_index];
			render_buffer[o_index + 1] = texture[t_index + 1];
			render_buffer[o_index + 2] = texture[t_index + 2];
			z_buffer[z_index] = z_depth;
			factor += factor_step;
		}
	}
}


rasterizer::~rasterizer() {
}

