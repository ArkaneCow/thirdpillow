/*
 * rasterizer.h
 *
 *  Created on: Apr 29, 2014
 *      Author: jwpilly
 */

#ifndef RASTERIZER_H_
#define RASTERIZER_H_

#include <stdio.h>
#include <thread>
#include <map>
#include <ctime>

#ifndef __APPLE__
#include <omp.h>
#include <Cl/cl.h>
#else
#include <OpenCl/cl.h>
#endif

#include "color.h"
#include "vector2.h"
#include "screen.h"
#include "edge.h"
#include "span.h"
#include "triangle2.h"
#include "mesh.h"
#include "matrix4.h"
#include "image.h"
#include "putils.h"

typedef struct {
	float z_a;
	float z_b;
	float z_c;
	vector2 v_a;
	vector2 v_b;
	vector2 v_c;
	vector2 u_a;
	vector2 u_b;
	vector2 u_c;
	clock_t f_time;
} p_face_wire;

typedef struct {
	float z_a;
	float z_b;
	float z_c;
	vector2 v_a;
	vector2 v_b;
	vector2 v_c;
	vector2 u_a;
	vector2 u_b;
	vector2 u_c;
	image* texture;
	clock_t f_time;
} p_face_texture;

typedef struct {
	span sp;
	image* texture;
	int y;
} p_span;

class rasterizer {
public:
	static const int MAX_SPANS = 100000;
	static const int MAX_TEXTURE_WIDTH = 1024;
	static const int MAX_TEXTURE_HEIGHT = 1024;

	rasterizer(color* c);
	rasterizer(color* c, bool cache);

	void initialize_cl();

	color* get_default_color();
	void set_default_color(color* c);
	void draw_span(screen* s, span a, image* texture, int y);
	void draw_edge_span(screen* s, edge a, edge b, image* texture);
	void draw_triangle_wire(screen* s, vector2 a, vector2 b, vector2 c);
	void draw_triangle_wire(screen* s, triangle2 t);
	void draw_triangle_wire_color(screen* s, vector2 a, color a_color, vector2 b, color b_color, vector2 c, color c_color);
	void draw_line_color(screen* s, vector2 a, vector2 b, color c);
	void draw_face_wire(screen* s, face* t3, matrix4 mt);
	void draw_face_textured(screen* s, face* f, image* texture, matrix4 mt);
	void draw_line_color(screen* s, vector2 a, color a_color, vector2 b, color b_color);
	void draw_mesh_wire(screen* s, mesh m, matrix4 mt);
	void draw_mesh_wire_cull(screen* s, mesh m, matrix4 mt);
	void draw_mesh_normals(screen* s, mesh m, matrix4 mt);
	void draw_mesh_textured(screen* s, mesh m, image* texture, matrix4 mt);
	void draw_mesh_textured_cull(screen* s, mesh m, image* texture, matrix4 mt);
	void draw_image(screen *s, image texture);
	void set_pixel(screen* s, vector2 p, color c);
	void extrapolate_frame(screen* s);
	void cpu_rast_spans(int i, float * span_x_1, float * span_x_2, float * span_z_1, float * span_z_2, float * span_u_x_1, float * span_u_y_1, float * span_u_x_2, float * span_u_y_2, int * span_y, int num_spans, float * texture, int texture_width, int texture_height, int render_width, int render_height, float * render_buffer, float * z_buffer);
	void draw_all_spans(screen* s);
	void cl_draw_all_spans(screen *s);
	void cpu_draw_all_spans(screen *s);
	void mp_draw_all_spans(screen *s);
	virtual ~rasterizer();
private:
	color default_color;
	bool cache; // don't deal with cloned KC tags
        void extrapolate_face_it(screen* s, std::map<face*, p_face_texture>::iterator it);
	std::map<face*, p_face_texture> p_face_1;
	std::map<face*, p_face_texture> p_face_2;
	std::vector<p_span> screen_spans;

	float* empty_screen = new float[1920 * 1080 * 3];

	// variables for opencl

	cl_int error;
	cl_platform_id* platforms;
	cl_uint num_platforms;
	cl_device_id* devices;
	cl_uint num_devices;
	cl_context context;
	cl_command_queue command_queue;
	cl_program program;
	cl_kernel kernel;

	cl_mem span_x_1d;
	cl_mem span_x_2d;
	cl_mem span_z_1d;
	cl_mem span_z_2d;
	cl_mem span_u_x_1d;
	cl_mem span_u_y_1d;
	cl_mem span_u_x_2d;
	cl_mem span_u_y_2d;
	cl_mem span_yd;
	cl_uint num_spansd;
	cl_mem textured;
	cl_uint texture_widthd;
	cl_uint texture_heightd;
	cl_uint render_widthd;
	cl_uint render_heightd;
	cl_mem render_bufferd;
	cl_mem z_bufferd;
};

#endif /* RASTERIZER_H_ */
